import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

export interface Item {
  id: number;
  title: string;
}

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private readonly items: Item[] = [{
    id: 1,
    title: 'Ham - Cooked'
  }, {
    id: 2,
    title: 'Pork - Loin, Boneless'
  }, {
    id: 3,
    title: 'Soup - Campbells, Creamy'
  }, {
    id: 4,
    title: 'Butter - Salted'
  }, {
    id: 5,
    title: 'Wine - Prosecco Valdobiaddene'
  }, {
    id: 6,
    title: 'Nut - Peanut, Roasted'
  }, {
    id: 7,
    title: 'Coconut - Creamed, Pure'
  }, {
    id: 8,
    title: 'Beans - Kidney, Red Dry'
  }, {
    id: 9,
    title: 'Chip - Potato Dill Pickle'
  }, {
    id: 10,
    title: 'Coffee Guatemala Dark'
  }, {
    id: 11,
    title: 'Beans - Fine'
  }, {
    id: 12,
    title: 'Island Oasis - Sweet And Sour Mix'
  }, {
    id: 13,
    title: 'Mussels - Cultivated'
  }, {
    id: 14,
    title: 'Lettuce - Curly Endive'
  }, {
    id: 15,
    title: 'Puree - Mango'
  }, {
    id: 16,
    title: 'Ezy Change Mophandle'
  }, {
    id: 17,
    title: 'Tomatoes - Cherry, Yellow'
  }, {
    id: 18,
    title: 'Cherries - Fresh'
  }, {
    id: 19,
    title: 'Potatoes - Purple, Organic'
  }, {
    id: 20,
    title: 'Shiro Miso'
  }];

  constructor() { }

  public getData(delayMs: number = 2000): Observable<Item[]> {
    return of(this.items)
    .pipe(delay(delayMs));
  }
}
