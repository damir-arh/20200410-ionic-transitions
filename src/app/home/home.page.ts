import { Component, OnInit, OnDestroy, ViewChildren, QueryList, ElementRef, ViewChild } from '@angular/core';
import { NavController, IonItem, IonContent, IonList } from '@ionic/angular';
import { DataService, Item } from '../data.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface ItemAnimationData {
  visible: boolean;
  position: 'above' | 'below';
  offset: number;
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {

  @ViewChild(IonContent, {static: true})
  private ionContent: IonContent;

  @ViewChild(IonList, { static: false, read: ElementRef })
  private ionList: ElementRef;

  @ViewChildren(IonItem, { read: ElementRef })
  private ionItems: QueryList<ElementRef>;

  public items: Item[];
  public itemAnimationData: ItemAnimationData[];
  public itemHeight = 0;
  public popOutAnimationDurationMs = 400;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private navController: NavController,
    private data: DataService
  ) {}

  public ngOnInit(): void {
    this.data.getData()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(items => {
      this.items = items;
    });
  }

  public ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public ionViewDidLeave() {
    this.itemAnimationData = undefined;
  }

  public pushDetail() {
    this.popOutItems().then(() => {
      setTimeout(() => {
        this.navController.navigateForward('/detail');
      }, this.popOutAnimationDurationMs);
    });
  }

  public getPopOutAnimationCssClass(index: number) {
    if (!this.itemAnimationData || !this.itemAnimationData[index].visible) {
      return '';
    }
    return this.itemAnimationData[index].position === 'above' ? 'popout-up' : 'popout-down';
  }

  private popOutItems() {
    return this.ionContent.getScrollElement().then(scrollElement => {
      const minYVisible = scrollElement.scrollTop;
      const maxYVisible = scrollElement.scrollTop + scrollElement.offsetHeight;
      const midYVisible = (maxYVisible + minYVisible) / 2;
      const listTop = this.ionList.nativeElement.offsetTop;

      let firstIndexVisible;
      let lastIndexVisible;
      const itemPositions: Pick<ItemAnimationData, 'visible' | 'position'>[] = this.ionItems.map((item, index) => {
        this.itemHeight = item.nativeElement.offsetHeight;
        const elementTop = listTop + item.nativeElement.offsetTop;
        const elementBottom = listTop + item.nativeElement.offsetTop + item.nativeElement.offsetHeight;
        const elementMiddle = (elementTop + elementBottom) / 2;
        const visible = elementBottom > minYVisible && elementTop < maxYVisible;
        const position = elementMiddle < midYVisible ? 'above' : 'below';
        if (visible) {
          lastIndexVisible = index;
          if (firstIndexVisible === undefined) {
            firstIndexVisible = index;
          }
        }
        return { visible, position };
      });

      this.itemAnimationData = itemPositions.map((item, index) => {
        return {
          ...item,
          offset: item.position === 'above' ? index - firstIndexVisible + 2 : lastIndexVisible - index + 2
        };
      });
    });
  }
}
