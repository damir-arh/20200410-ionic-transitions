import { Component, OnInit, ViewChildren, ElementRef, QueryList, ViewChild } from '@angular/core';
import { ModalController, IonCard, IonContent } from '@ionic/angular';
import { createAnimation } from '@ionic/core';
import { ModalPage } from '../modal/modal.page';
import { modalEnterAnimation } from '../animations/modal-animation';

interface ImageData {
  blurhash: string;
  filename: string;
  url?: string;
}

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  public readonly images: ImageData[] = [
    { blurhash: 'LDG8Dg-p04?F},n#JBWC0kt7=_E2', filename: 'photo1.jpg' },
    { blurhash: 'L6B5aA$+R*Zmq4J5tPoy^*-.xtMN', filename: 'photo2.jpg' },
    { blurhash: 'L4B:Tz~pAI0100~p4o0Ku4D*-T-:', filename: 'photo3.jpg' },
    { blurhash: 'LfLNY[tR%#RPobM|V@%L~qbcxaof', filename: 'photo4.jpg' }
  ];

  @ViewChildren('image', {read: ElementRef})
  private imageElements: QueryList<ElementRef<HTMLElement>>;

  @ViewChildren(IonCard, {read: ElementRef})
  private cardElements: QueryList<ElementRef<HTMLElement>>;

  @ViewChild(IonContent, {read: ElementRef})
  private contentElement: ElementRef<HTMLElement>;

  constructor(
    private modalCtrl: ModalController,
    private pageElement: ElementRef<HTMLElement>
  ) { }

  public ngOnInit(): void {
    // artificially delay image loading to show blurhash for longer time
    this.images.forEach(image => {
      setTimeout(() => {
        image.url = this.getImageUrl(image.filename);
      }, 800 + Math.random() * 400);
    });
  }

  public async openModal(index: number) {

    const resetAnimation = await this.playAnimation(index);

    const modal = await this.modalCtrl.create({
      component: ModalPage,
      componentProps: {
        url: this.getImageUrl(this.images[index].filename)
      },
      enterAnimation: modalEnterAnimation
    });
    await modal.present();

    resetAnimation();
  }

  private async playAnimation(index: number) {

    const imageElement = this.imageElements.toArray()[index].nativeElement;

    const firstRect = imageElement.getBoundingClientRect();

    const clone = imageElement.cloneNode(true) as HTMLElement;
    this.pageElement.nativeElement.appendChild(clone);
    clone.classList.add('maximized');
    clone.classList.remove('animate');
    const lastRect = clone.getBoundingClientRect();

    const invert = {
      translateX: firstRect.left - lastRect.left,
      translateY: firstRect.top - lastRect.top,
      scaleX: firstRect.width / lastRect.width,
      scaleY: firstRect.height / lastRect.height
    };

    const imageAnimation = createAnimation()
      .addElement(clone)
      .beforeStyles({
        'transform-origin': '0 0'
      })
      .fromTo(
        'transform',
        `translate(${invert.translateX}px, ${invert.translateY}px) scale(${invert.scaleX}, ${invert.scaleY})`,
        'translate(0, 0) scale(1, 1)'
      );

    const cardElement = this.cardElements.toArray()[index].nativeElement;
    cardElement.classList.add('hidden');

    const contentAnimation = createAnimation()
      .addElement(this.contentElement.nativeElement)
      .fromTo(
        'transform',
        'translateX(0)',
        `translateX(-${this.contentElement.nativeElement.offsetWidth}px)`
      );

    const parentAnimation = createAnimation()
      .duration(300)
      .easing('ease-in-out')
      .addAnimation([imageAnimation, contentAnimation]);

    await parentAnimation.play();

    return () => {
      clone.remove();
      cardElement.classList.remove('hidden');
      parentAnimation.stop();
    };
  }

  private getImageUrl(filename: string): string {
    return `assets/img/${filename}`;
  }
}
