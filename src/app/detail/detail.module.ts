import { PageShadowModule } from '../page-shadow/page-shadow.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailPageRoutingModule } from './detail-routing.module';

import { DetailPage } from './detail.page';
import { BlurhashImgComponentModule } from '../blurhash-img/blurhash-img.module';
import { ModalPageModule } from '../modal/modal.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailPageRoutingModule,
    PageShadowModule,
    BlurhashImgComponentModule,
    ModalPageModule
  ],
  declarations: [DetailPage]
})
export class DetailPageModule {}
