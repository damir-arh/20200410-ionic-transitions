import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BlurhashImgComponent } from './blurhash-img.component';

@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule,],
  declarations: [BlurhashImgComponent],
  exports: [BlurhashImgComponent]
})
export class BlurhashImgComponentModule {}
