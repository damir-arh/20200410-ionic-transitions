import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { decode } from 'blurhash';

@Component({
  selector: 'app-blurhash-img',
  templateUrl: './blurhash-img.component.html',
  styleUrls: ['./blurhash-img.component.scss'],
})
export class BlurhashImgComponent implements AfterViewInit {

  private blurHashValue: string;
  @Input()
  get blurHash(): string {
    return this.blurHashValue;
  }
  set blurHash(value: string) {
    this.blurHashValue = value;
    this.decodeBlurHash();
  }

  private imageSrcValue: string;
  @Input()
  get imageSrc(): string {
    return this.imageSrcValue;
  }
  set imageSrc(value: string) {
    this.imageSrcValue = value;
  }

  @ViewChild('canvas', {static: true})
  private canvas: ElementRef<HTMLCanvasElement>;

  public canvasWidth = 32;
  public canvasHeight = 32;
  public imageLoaded = false;

  ngAfterViewInit() {
    this.decodeBlurHash();
  }

  private decodeBlurHash() {
    if (this.canvas && this.blurHash) {
      const context = this.canvas.nativeElement.getContext('2d');
      const imageData = context.createImageData(this.canvasWidth, this.canvasHeight);
      const pixels = decode(this.blurHash, this.canvasWidth, this.canvasHeight);
      imageData.data.set(pixels);
      context.putImageData(imageData, 0, 0);
    }
  }

}
