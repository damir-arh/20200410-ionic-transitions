import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BlurhashImgComponent } from './blurhash-img.component';

describe('BlurhashImgComponent', () => {
  let component: BlurhashImgComponent;
  let fixture: ComponentFixture<BlurhashImgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlurhashImgComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BlurhashImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
