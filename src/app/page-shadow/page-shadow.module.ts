import { PageShadowComponent } from './page-shadow.component';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [PageShadowComponent],
    exports: [PageShadowComponent]
})
export class PageShadowModule {}
