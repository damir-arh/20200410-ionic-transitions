import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PageShadowComponent } from './page-shadow.component';

describe('PageContainerComponent', () => {
  let component: PageShadowComponent;
  let fixture: ComponentFixture<PageShadowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageShadowComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PageShadowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
