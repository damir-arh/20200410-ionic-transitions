# List item pop-in and pop-out animations

Implemented sample animations:

![List animations](docs/list-animation.gif)

**Note:**

Sample code uses the following syntax for binding values to CSS variables which only works in Angular 9:

```
[style.--animation-order]="i"
```

In Angular 8, the following alternative syntax must be used (`sanitizer` is an instance of `DomSanitizer`):

```
[attr.style]="sanitizer.bypassSecurityTrustStyle('--animation-order: ' + i)"
```

# BlurHash placeholder for images

Sample implementation of [BlurHash](https://blurha.sh/) with transition for images that are still loading:

![BlurHash transition](docs/blurhash.gif)

# Page transition

Implemented sample page transition:

![Page transition animation](docs/page-transition.gif)

The same page transition in slow motion:

![Page transition animation in slow motion](docs/page-transition-slo-mo.gif)

# Modal transition

Sample custom modal transition that is preceded by an animation on the previous page which leads up to it:

![Modal transition](docs/modal-transition.gif)
